from urllib.parse import urlencode
import json

def call(client, path, params):
    url = path
    response = client.post(url,data=params)
    return json.loads(response.data.decode('utf-8'))

def test_plus_one(client):
    result = call(client, '/', {'a': 20, 'b': '10'})
    assert result['hasil'] == 30
