from flask import request, Flask
import json

app = Flask(__name__)

@app.route('/', methods=['POST'])
def plus_operator():
    print(request)
    a = int(request.form['a'])
    b = int(request.form['b'])
    return json.dumps({'a': a, 'b': b, 'hasil': a+b})
