# Assignment 1 (Backend with Flask API)
## Adrian Wijaya - 1806205363

## To run in local
Activate environment file first

`virtualenv env`

then

`source env/bin/activate`

To run the in local:

`python3 -m flask run`

